#include <algorithm>
#include <iostream>
#include <vector>

using std::cin;
using std::cout;
using std::endl;
using std::vector;

struct Node {
  int key;
  int left;
  int right;

  Node() : key(0), left(-1), right(-1) {}
  Node(int key_, int left_, int right_) : key(key_), left(left_), right(right_) {}
};

bool CheckBinarySearchTree(const vector<Node>& tree, int index, int from, int to) {
	bool left = 1, right = 1;
	if (tree[index].key <=from || tree[index].key >= to)
		return 0;
	if (tree[index].left != -1)
		left = CheckBinarySearchTree(tree, tree[index].left, from, tree[index].key);
	if (tree[index].right != -1)
		right = CheckBinarySearchTree(tree, tree[index].right, tree[index].key, to);
	return left && right;
}

bool IsBinarySearchTree(const vector<Node>& tree) {
  long int from = -2147483647 - 1;
  long int to = 2147483647;
  if(!tree.empty())
	  if (!CheckBinarySearchTree(tree, 0, from, to))
		  return false;
  return true;
}

int main() {
  int nodes;
  cin >> nodes;
  vector<Node> tree;
  for (int i = 0; i < nodes; ++i) {
    int key, left, right;
    cin >> key >> left >> right;
    tree.push_back(Node(key, left, right));
  }
  if (IsBinarySearchTree(tree)) {
    cout << "CORRECT" << endl;
  } else {
    cout << "INCORRECT" << endl;
  }
  return 0;
}
