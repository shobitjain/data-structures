#include <iostream>
#include <stack>
#include <string>

struct Bracket {
    Bracket(char type, int position):
        type(type),
        position(position)
    {}

    bool Matchc(char c) {
        if (type == '[' && c == ']')
            return true;
        if (type == '{' && c == '}')
            return true;
        if (type == '(' && c == ')')
            return true;
        return false;
    }

    char type;
    int position;
};

int main() {
    std::string text;
    getline(std::cin, text);

    std::stack <Bracket> opening_brackets_stack;
	int tempposition=0;
    for (int position = 0; position < text.length(); ++position) {
        char next = text[position];
        if (next == '(' || next == '[' || next == '{') {
            // Process opening bracket, write your code here
			Bracket b(next,position);
            opening_brackets_stack.push(b);
        }

        if (next == ')' || next == ']' || next == '}') {
			if (opening_brackets_stack.empty()) // fail for } or } or )
			{
				tempposition = position+1;
                break;
			}
			if (opening_brackets_stack.top().Matchc(next))
			{
				opening_brackets_stack.pop();
			}
			else
			{
				tempposition = ++position;
                break;
			}
        }
        if(position == (text.length())-1 && !opening_brackets_stack.empty()) 
        {
            tempposition = opening_brackets_stack.top().position+1;
        }
    }

    // Printing answer, write your code here
	if (tempposition != 0)
	{
        std::cout << tempposition;
		//printf("%d",tempposition);
	}
	else
	{
        std::cout << "Success";
		//printf("Success");
	}

    return 0;
}