#include <iostream>
#include <vector>
#include <algorithm>
#include <queue>

using std::vector;
using std::cin;
using std::cout;

class Operator {
	public:
		int id;
        long long comingFreeTime;
	      Operator (int id) {
            this->id = id;
            comingFreeTime = 0;
        }
};

struct OperatorCompare {
	bool operator()(const Operator &o1, const Operator &o2) const {
		if(o1.comingFreeTime == o2.comingFreeTime)
			return o1.id > o2.id;
		else
            return o1.comingFreeTime > o2.comingFreeTime;
	}
};

class JobQueue {
 private:
  int num_workers_;
  vector<int> jobs_;

  vector<int> assigned_workers_;
  vector<long long> start_times_;

  void WriteResponse() const {
    for (int i = 0; i < jobs_.size(); ++i) {
      cout << assigned_workers_[i] << " " << start_times_[i] << "\n";
    }
  }

  void ReadData() {
    int m;
    cin >> num_workers_ >> m;
    jobs_.resize(m);
    for(int i = 0; i < m; ++i)
      cin >> jobs_[i];
  }

  void AssignJobs() {
    // TODO: replace this code with a faster algorithm.
    assigned_workers_.resize(jobs_.size());
    start_times_.resize(jobs_.size());
    std::priority_queue<Operator, vector<Operator>, OperatorCompare> priorityQueue;
      for(int i = 0; i < num_workers_; i++) 
      {
        priorityQueue.push(Operator(i));
      }
      for (int i = 0; i < jobs_.size(); i++) 
      {
        Operator openThread = priorityQueue.top();
        priorityQueue.pop();
        assigned_workers_[i] = openThread.id;
        start_times_[i] = openThread.comingFreeTime;
        openThread.comingFreeTime = openThread.comingFreeTime + jobs_[i];
        priorityQueue.push(openThread);
      }
  }

 public:
  void Solve() {
    ReadData();
    AssignJobs();
    WriteResponse();
  }
};

int main() {
  std::ios_base::sync_with_stdio(false);
  JobQueue job_queue;
  job_queue.Solve();
  return 0;
}